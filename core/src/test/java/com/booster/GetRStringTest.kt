package com.booster

import android.content.Context
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

private const val FAKE_STRING = "HELLO WORLD"

@RunWith(MockitoJUnitRunner::class)
class GetRStringTest {
    @Mock
    private lateinit var mockContext: Context

    @Test
    fun readStringFromContext_LocalizedString() {
        // Given a mocked Context injected into the object under test...
        whenever(mockContext.getString(R.string.app_name)).thenReturn(FAKE_STRING) // 想测试不相等就可以返回 ("Booster")
        val getRString = GetRString(mockContext)
        // ...when the string is returned from the object under test...
        val result = getRString.getHelloWorldString()
        // ...then the result should be the expected one.
        println("$getRString = $result")
        // 期望是一样的，如果不一样，就报警后面的msg
        assertEquals(FAKE_STRING, result, "获取到的 hello world 字符串不相同")
    }
}