package com.booster.utility

import android.util.Base64
import com.nhaarman.mockitokotlin2.whenever
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class EasyAESTest {
    companion object {
        @BeforeClass
        fun setup() {
            // Test 的单元测试启动前设置 MmkvLogUtil 处于测试状态，返回 mmkv 为空
            // AndroidTest 因为有 android 做支持，不用这样设置
            MmkvLogUtil.isInTestStatus = true
        }

        @AfterClass
        fun tearDown() {
            MmkvLogUtil.isInTestStatus = false // 测试启动前恢复 MmkvLogUtil 不处于测试状态
        }
    }

    @Test
    fun testEncrypt() {
        // 使用 assertEquals
        var content = "..我来测试\n1English1$$" // 尝试字符串里包括中文，英文字符，特殊字符，换行符
        var encryptContent = "ZDzE+EQiqx2sDIEP4lvT4fT8ZAmgFKh7L66ud4ez2vQ=\n" // 加密出来的字符串总是会在最后面加上 \n ，不知为何
        var encryptResult = EasyAES.encryptString256(content)
        var decryptResult = EasyAES.decryptString256(encryptResult)
        assertEquals(encryptContent, encryptResult, "EasyAes 加密出错")
        assertEquals(content, decryptResult, "EasyAes 解密出错")

        content = "我爱浩崽崽"
        encryptContent = "egf+jq8zxTJQzm6PmBu0PQ==\n"
        encryptResult = EasyAES.encryptString256(content)
        decryptResult = EasyAES.decryptString256(encryptResult)
        assertEquals(encryptContent, encryptResult, "EasyAes 加密出错")
        assertEquals(content, decryptResult, "EasyAes 解密出错")
    }
}