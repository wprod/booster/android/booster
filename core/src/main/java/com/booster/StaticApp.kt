package com.booster

import android.app.Application
import com.booster.utility.NotNullSingleValueVar
import com.github.kittinunf.fuel.core.FuelManager
import com.leon.channel.helper.ChannelReaderUtil
import com.tencent.mmkv.MMKV

private const val TAG = "StaticApp"

class StaticApp : Application() {
    companion object {
        private var instance: StaticApp by NotNullSingleValueVar()
        fun instance() = instance
    }

    val channel: String by lazy {
        val mmkvDefault = MMKV.defaultMMKV()
        val key = "$TAG-channel"
        var value = mmkvDefault.decodeString(key)
        if (value.isNullOrBlank()) {
            value = appChannel()
            mmkvDefault.encode(key, value)
        }
        value
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initComponent()
    }

    private fun initComponent() { // 初始化全局使用的组件，如 MMKV  Fuel
        val mmkvRootDir = MMKV.initialize(this)
        // MMKV 支持 API level 16 以上平台；来自： https://github.com/Tencent/MMKV/wiki/android_setup_cn
        println("MMKV 保存数据的根目录是：$mmkvRootDir")
        // 超时时间 3 秒
        FuelManager.instance.timeoutInMillisecond = 3000
    }

    /**
     * 从 ChannelReaderUtil 中获取渠道值
     * 蓝鲸的渠道是 1, 2, 40056, 40100 至 40330
     */
    private fun appChannel(): String {
        val channel = ChannelReaderUtil.getChannel(applicationContext)
        return if (channel.isNullOrBlank()) {
            "40100" // 如果没有分离出渠道号，就归为官方渠道 1 , 测试渠道 2， 其它渠道为 40100 - 40330
        } else {
            val prefix = "lanjing-"
            if (channel.contains(prefix)) channel.replace(prefix, "") else channel
        }
    }
}