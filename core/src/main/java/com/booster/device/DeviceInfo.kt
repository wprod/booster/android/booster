package com.booster.device

import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import com.booster.StaticApp
import com.booster.utility.MmkvLog
import com.booster.utility.saveMmkvLog
import com.booster.utility.md5
import com.tencent.mmkv.MMKV
import com.tencent.mmkv.MMKVLogLevel
import java.io.UnsupportedEncodingException
import java.lang.reflect.Field
import java.net.URLEncoder
import java.util.*

private const val TAG = "DeviceInfo"

object DeviceInfo {
    val uuid: String by lazy {
        val mmkvDefault = MMKV.defaultMMKV()
        val key = "$TAG-uuid"
        var value = mmkvDefault.decodeString(key)
        if (value.isNullOrBlank()) {
            value = deviceUuid()
            mmkvDefault.encode(key, value)
        }
        value
    }
    val desc: String by lazy {
        val mmkvDefault = MMKV.defaultMMKV()
        val key = "$TAG-uuid"
        var value = mmkvDefault.decodeString(key)
        if (value.isNullOrBlank()) {
            value = deviceDesc()
            mmkvDefault.encode(key, value)
        }
        value
    }
}

// 获取设备唯一 ID
private fun deviceUuid(): String {
    val uuid = getMd5OfDeviceUuid()
    return if (uuid.isNullOrBlank()) { // 如果仍然没有获取到，就把默认的给服务器，服务器可以判断是否获取 uui 失效了
        val func = "deviceUuid"
        saveMmkvLog {
            val msg = "getMd5OfDeviceUuid()，返回空值"
            MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
        }
        "PEP8j9ic6vc6eqnyTwnIu4tb1tuzc130qzo"
    } else {
        uuid
    }
}

/**
 * 获取设备序列号 一些列硬件信息拼装获取到的内容，组合出来计算出 MD5 进行保存
 */
private fun getMd5OfDeviceUuid(): String? { // 获取设备序列号不需要权限，但是有一定的局限性，在有些手机上会出现垃圾数据，比如红米手机返回的就是连续的非随机数
    val androidId: String = Settings.System.getString(
        StaticApp.instance().contentResolver,
        Settings.System.ANDROID_ID
    )
    val uniquePsuedoID = getUniquePsuedoID() // 一些列硬件信息拼装获取到的内容
    return "$uniquePsuedoID$androidId".md5()
}

/**
 * 一些列硬件信息拼装获取到的内容 原文链接：https://blog.csdn.net/dqliangjun/article/details/79120776
 */
private fun getUniquePsuedoID(): String {
    val szDevIDShort = "35" + Build.BOARD.length % 10 + Build.BRAND.length % 10 + Build.CPU_ABI.length % 10 +
                Build.DEVICE.length % 10 + Build.MANUFACTURER.length % 10 + Build.MODEL.length % 10 + Build.PRODUCT.length % 10
    // Thanks to @Roman SL!
    // http://stackoverflow.com/a/4789483/950427
    // Only devices with API >= 9 have android.os.Build.SERIAL
    // http://developer.android.com/reference/android/os/Build.html#SERIAL
    // If a logInUser upgrades software or roots their phone, there will be a duplicate entry
    var serial = ""
    try {
        val `object` = Build::class.java.getField("SERIAL")[null]
        if (`object` != null) {
            serial = `object`.toString()
        }
        // Go ahead and return the serial for api => 9
        return UUID(szDevIDShort.hashCode().toLong(), serial.hashCode().toLong()).toString()
    } catch (exception: Exception) { // String needs to be initialized
        serial = "serial" // some value
    }
    // Thanks @Joe!
    // http://stackoverflow.com/a/2853253/950427
    // Finally, combine the values we have found by using the UUID class to create a unique identifier
    return UUID(szDevIDShort.hashCode().toLong(), serial.hashCode().toLong()).toString()
}

// 获取设备硬件描述
private fun deviceDesc(): String? {
    val map = hardwareInfo() // 收集设备参数信息
    val stringBuilder = StringBuilder()
    if (!map.isNullOrEmpty()) {
        for (entry in map.entries) {
            val key = entry.key
            var value = entry.value
            if (!TextUtils.isEmpty(value)) {
                value = value.replace("\"".toRegex(), "") // 还是要清理掉这个 " 引号，否则 json 格式容易出错
                if (stringBuilder.isNotEmpty()) { // 不要用空格，否则 url 编码后解码，空格的地方会出来 +
                    stringBuilder.append(",\n")
                }
                stringBuilder.append("\"").append(key).append("\":\"").append(value)
                    .append("\"")
            }
        }
        if (stringBuilder.isNotEmpty()) {
            var value = stringBuilder.toString()
            try {
                value = URLEncoder.encode(value, "UTF-8")
            } catch (exception: UnsupportedEncodingException) {
                exception.printStackTrace()
                val func = "hardwareInfo 中的 convert"
                saveMmkvLog {
                    MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
                }
            }
            return "{\n$value\n}"
        }
    }
    return null
}

/**
 * 收集设备参数信息
 */
private fun hardwareInfo(): Map<String, String>? {
    // 函数中的函数
    fun convert(accessibleFieldList: List<Field>, map: MutableMap<String, String>) {
        try {
            for (field in accessibleFieldList) {
                val value = field.get(null)
                if (value != null) {
                    map[field.name] = value.toString()
                }
            }
        } catch (exception: java.lang.Exception) { // 因为 field.get(null) 有可能会弹出异常
            exception.printStackTrace()
            val func = "hardwareInfo 中的 convert"
            saveMmkvLog {
                MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
            }
        }
    }

    val deviceInfoMap: MutableMap<String, String> = HashMap()
    var fieldList = Build::class.java.declaredFields.toList()
    fieldList.forEach { it.isAccessible = true } // 预先设置这个可访问
    // 转换这个列表中的数据到 map 中，正方向的，万一其中出错了，下面还有一个是反向
    fieldList = fieldList.filter { it.get(null) != null }
    convert(fieldList, deviceInfoMap)
    // 转换这个列表中的数据到 map 中，反方向的，尽可能收集齐全设备硬件信息
    val fieldReverseList = fieldList.reversed()
    convert(fieldReverseList, deviceInfoMap)
    return deviceInfoMap
}