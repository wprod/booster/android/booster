package com.booster.domain.useCase

import com.booster.domain.entity.UserEntity
import com.booster.domain.entity.ValidityEntity
import com.booster.domain.repository.Repository.Companion.nowAndToday0ClockTimeStamp
import com.booster.domain.repository.UserRepository
import com.booster.utility.MmkvLog
import com.booster.utility.saveMmkvLog
import com.tencent.mmkv.MMKVLogLevel

interface UserUseCase { // 参考：[Kotlin 接口](https://www.kotlincn.net/docs/reference/interfaces.html)
    fun current(): UserEntity?                                          // 获取当前已经登录的用户数据
    // logIn() 没有当前用户，则用 device id 直接登录，返回用户数据，有当前用户，且今天未曾登录成功，则调用日活接口一次
    fun logIn(): Triple<Boolean, String?, UserEntity?>

    fun logIn(
        name: String,
        pwd: String
    ): Triple<Boolean, String?, UserEntity?>    // 用户输入的账号密码登录，返回用户数据

    fun logOut()                                                        // 注销当前用户，如果有显示用的用户名，就保留，让用户不用再输入
    fun getValidity(): Triple<Boolean, String?, ValidityEntity?>        // 获取当前用户的有效期，成功则保存到本地
    fun patchPwd(pwd: String): Triple<Boolean, String?, UserEntity?>    // 更新当前用户的密码，成功则保存到本地
}

private const val TAG = "DefaultUserUseCase"

// 接口 UserUseCase 有个默认的实现
class DefaultUserUseCase(private val repository: UserRepository) : UserUseCase {

    override fun current(): UserEntity? {
        return repository.load()
    }

    // logIn() 没有当前用户，则用 device id 直接登录，返回用户数据，有当前用户，且今天未曾登录成功，则调用日活接口一次
    override fun logIn(): Triple<Boolean, String?, UserEntity?> {
        val current = current()
        val nameFromServer = current?.nameFromServer
        val inputName = current?.inputName
        val inputPwd = current?.inputPwd
        return if (current == null || nameFromServer.isNullOrBlank() || inputPwd.isNullOrBlank()) {
            val result = repository.get()   // 用 device id 注册或获取用户资料
            val (ok, msg, user) = result
            if (ok && user != null) {
                user.logInTime = System.currentTimeMillis()
                repository.save(user)
            } else {
                val msg2 = if (msg.isNullOrBlank()) "免注册登录出错" else msg
                saveMmkvLog {
                    val func = "logIn() 免注册登录"
                    MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg2)
                }
            }
            result
        } else {
            val logInTime = current.logInTime
            val (_, today0Clock) = nowAndToday0ClockTimeStamp()
            if (logInTime < today0Clock) { // 登录成功的时间戳不是今天 0 点以后的
                val result = repository.get(nameFromServer, inputPwd, true)
                val (ok, msg, user) = result
                if (ok && user != null) { // 登录成功，保留输入用户名，更新用户数据和登录时刻，保存在本地
                    user.inputName = current.inputName
                    user.logInTime = System.currentTimeMillis()
                    repository.save(user)
                    result
                } else {
                    val msg2 = if (msg.isNullOrBlank()) {
                        if (inputName.isNullOrBlank()) "日活登录出错，尝试免注册登录" else "日活登录出错"
                    } else msg
                    saveMmkvLog {
                        val func = "logIn() 日活登录"
                        MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg2)
                    }
                    if (inputName.isNullOrBlank()) { // 登录失败，当前用户没有输入过用户名
                        repository.clear() // 清除当前用户，重新用 device id 获取用户资料
                        logIn()
                    } else result // 登录失败，当前用户输入过用户名，返回出错，弹出界面让用户输入账号密码登录
                }
            } else {
                Triple(true, null, current)
            }
        }
    }

    override fun logIn(name: String, pwd: String): Triple<Boolean, String?, UserEntity?> {
        val result = repository.get(name, pwd)
        val (ok, msg, user) = result
        if (ok && user != null) {
            user.inputName = name
            user.logInTime = System.currentTimeMillis()
            repository.save(user)
        } else {
            val msg2 = if (msg.isNullOrBlank()) "用户名密码登录出错" else msg
            saveMmkvLog {
                val func = "logIn(name, pwd)"
                MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg2)
            }
        }
        return result
    }

    override fun logOut() {
        val user = current()    // 获取当前用户
        repository.clear()      // 清除全部用户记录
        val name = user?.inputName // 如果有显示用的用户名，就保留，让用户不用再输入
        if (!name.isNullOrBlank()) {
            repository.save(UserEntity(inputName = name))
        }
    }

    override fun getValidity(): Triple<Boolean, String?, ValidityEntity?> {
        val id = current()?.id // 获取当前用户 id
        return if (id != null) repository.getValidity(id) else {
            val msg = "用户未登录"
            saveMmkvLog {
                val func = "getValidity()"
                MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
            }
            Triple(false, msg, null)
        }
    }

    override fun patchPwd(pwd: String): Triple<Boolean, String?, UserEntity?> {
        val current = current()
        val id = current?.id  // 获取当前用户 id
        var msg = "用户未登录"
        return if (current == null || id == null) {
            saveMmkvLog {
                val func = "patchPwd(pwd)"
                MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
            }
            Triple(false, msg, null)
        } else {
            val result = repository.patchPwd(id, pwd)
            val (ok, msg2, user) = result
            if (!ok || user == null || user.pwdFromServer.isNullOrBlank()) {
                msg = if (msg2.isNullOrBlank()) "修改密码出错或返回空值" else msg2
                saveMmkvLog {
                    val func = "patchPwd(pwd)"
                    MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
                }
                Triple(false, msg, null)
            } else {
                current.inputPwd = pwd
                current.pwdFromServer = user.pwdFromServer
                repository.save(current)
                Triple(true, null, current)
            }
        }
    }
}