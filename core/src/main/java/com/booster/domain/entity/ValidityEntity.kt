package com.booster.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class ValidityEntity(
    val deviceSignInToday: Int?,// 今天已经登录了几台设备，多于 1 台需要提示用户
    val expiry: Long?,          // 到期时间戳
    val desc: String?           // 服务器给的有效期描述
) : Parcelable

fun ValidityEntity.expiried(): Boolean {
    return if (expiry == null) true else System.currentTimeMillis() > expiry
}