package com.booster.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize // [kotlin使用Parcelize注解简化Parcelable的书写](https://juejin.im/entry/5a261a8c6fb9a0450167cf1b)
data class UserEntity( // 参考：[Kotlin 数据类](https://www.kotlincn.net/docs/reference/data-classes.html)
    var id: Long? = null,                   // 如果生成的类需要含有一个无参的构造函数，则所有的属性必须指定默认值。
    var inputName: String? = null,          // 用户登陆时输入的用户名，一定是有的，否则无法登录或做其它操作
    var inputPwd: String? = null,           // 用户登陆时输入的密码
    var nameFromServer: String? = null,     // 登录成功服务器返回的用户名
    var pwdFromServer: String? = null,      // 登录成功服务器返回的密码
    var validity: ValidityEntity? = null,   // 有效期
    var logInTime: Long = 0                 // 登录的时间戳
) : Parcelable