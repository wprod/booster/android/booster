package com.booster.domain.repository

import com.booster.data.local.UserLocalData
import com.booster.data.remote.UserRemoteData
import com.booster.data.remote.ValidityRemoteData
import com.booster.domain.entity.UserEntity
import com.booster.domain.entity.ValidityEntity
import com.booster.utility.MmkvLog
import com.booster.utility.saveMmkvLog
import com.tencent.mmkv.MMKVLogLevel

interface UserRepository {
    fun load(): UserEntity?     // 载入当前用户
    fun save(user: UserEntity)  // 保存为当前用户，
    fun clear()                 // 清除当前用户，如果有显示用的用户名，就保留，让用户不用再输入
    fun get(): Triple<Boolean, String?, UserEntity?> // 自动注册本机 device id 为新用户，以后获取 device id 对应的用户对象
    // isNameFromServer false 代表用户输入的账号密码去获取用户对象 isNameFromServer true 代表登录成功后第二天 app，用当前用户的服务器返回的账号密码获取用户对象
    fun get(
        name: String,
        pwd: String,
        isNameFromServer: Boolean = false
    ): Triple<Boolean, String?, UserEntity?>

    fun getValidity(id: Long): Triple<Boolean, String?, ValidityEntity?>    // 获取当前用户的有效期
    fun patchPwd(
        id: Long,
        pwd: String
    ): Triple<Boolean, String?, UserEntity?>  // 更改当前用户的密码，返回服务器保存的新密码
}

private const val TAG = "DefaultUserRepository"

class DefaultUserRepository(
    private val localData: UserLocalData,
    private val remoteData: UserRemoteData,
    private val validityRemoteData: ValidityRemoteData
) : UserRepository { // 接口 UserUseCase 有个默认的实现

    override fun load(): UserEntity? {
        val (ok, _, userList) = localData.get()
        return if (!ok || userList == null || userList.isEmpty()) {
            null
        } else userList[0]
    }

    override fun save(user: UserEntity) {
        localData.put(user)
    }

    override fun clear() {
        val (ok, _, userList) = localData.get()
        if (ok && userList != null && userList.isNotEmpty()) {
            for (user in userList) {
                val id = user.id
                if (id != null) {
                    localData.delete(id)
                }
            }
        }
    }

    override fun get(): Triple<Boolean, String?, UserEntity?> {
        val (ok, msg, userList) = remoteData.get()
        return if (!ok || userList.isNullOrEmpty() || userList[0].id == null) {
            val msg2 = if (msg.isNullOrBlank()) "免注册登录出错或返回空值" else msg
            saveMmkvLog {
                val func = "get() 免注册登录"
                MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg2)
            }
            Triple(ok, msg, null)
        } else {
            Triple(ok, msg, userList[0])
        }
    }

    override fun get(
        name: String,
        pwd: String,
        isNameFromServer: Boolean
    ): Triple<Boolean, String?, UserEntity?> {
        val parameterList = mutableListOf("name" to name, "pwd" to pwd)
        if (isNameFromServer) { // 默认是账号密码登录，如果是服务器返回的账号，则代表是日活登录
            parameterList.add("nameFromServer" to "true")
        }
        val result = remoteData.get(parameterList)
        val (ok, msg, userList) = result
        return if (!ok || userList.isNullOrEmpty()) {
            val msg2 = if (msg.isNullOrBlank()) {
                if (isNameFromServer) "日活登录出错或返回空值" else "账号密码登录出错或返回空值"
            } else msg
            saveMmkvLog {
                val func =
                    "get(name, pwd, isNameFromServer) ${if (isNameFromServer) "日活登录" else "账号密码登录"}"
                MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg2)
            }
            Triple(ok, msg, null)
        } else Triple(ok, msg, userList[0])
    }

    override fun getValidity(id: Long): Triple<Boolean, String?, ValidityEntity?> {
        return validityRemoteData.get(id)
    }

    override fun patchPwd(id: Long, pwd: String): Triple<Boolean, String?, UserEntity?> {
        return remoteData.patch(UserEntity(id = id, inputPwd = pwd))
    }
}