package com.booster.domain.repository

import java.security.SecureRandom
import java.util.*

interface Repository<TYPE> {
    companion object {
        const val SHORT_APP_NAME = "ljo"    // 项目名称 蓝鲸open 的缩写

        // 以下是判断时间戳是否在一定范围
        const val MillisSecondOfHour =
            60 * 60 * 1000.toLong() // 1小时的毫秒数now - now % MillisSecondOnow > Today4Clockf24Hour
        const val MillisSecondOf24Hour = 24 * MillisSecondOfHour // 24小时的毫秒数
        var Today0Clock: Long = 0 // 今天 0 点时的时间戳
        var Today4Clock: Long = 0 // 今天 4 点时的时间戳
        var Today22Clock: Long = 0 // 今天 22 点时的时间戳

        fun nowAndToday0ClockTimeStamp() : Pair<Long, Long> {
            val now = System.currentTimeMillis()
            val today0Clock = now - now % MillisSecondOf24Hour - 8 * MillisSecondOfHour // 北京时间是 8 时区
            return Pair(now, today0Clock)
        }

        fun canUpdate(): Boolean { // 当前不在 今天 22 点和凌晨4点之间
            val (now, today0Clock) = nowAndToday0ClockTimeStamp()
            if (today0Clock != Today0Clock) {
                Today0Clock = today0Clock
                Today4Clock = Today0Clock + 4 * MillisSecondOfHour
                Today22Clock = Today0Clock + 22 * MillisSecondOfHour
            }
            return now > Today4Clock && now < Today22Clock
        }

        fun needUpdateSince(lastUpdateTimeStamp: Long): Boolean { // lastUpdateTimeStamp<0 表示从未更新过，或距离上次更新 大于 24 小时 和 当前不在 今天 22 点和凌晨4点之间
            val (now, today0Clock) = nowAndToday0ClockTimeStamp()
            if (today0Clock != Today0Clock) {
                Today0Clock = today0Clock
                Today4Clock = Today0Clock + 4 * MillisSecondOfHour
                Today22Clock = Today0Clock + 22 * MillisSecondOfHour
            }
            return lastUpdateTimeStamp <= 0 || now > Today4Clock && now < Today22Clock && now - lastUpdateTimeStamp > MillisSecondOf24Hour
        }

        fun needUpdateSince(
            lastUpdateTimeStamp: Long,
            intervalHour: Float
        ): Boolean { // lastUpdateTimeStamp<0 表示从未更新过，或距离上次更新 大于 intervalHour 个小时 和 当前不在 今天 22 点和凌晨4点之间
            val (now, today0Clock) = nowAndToday0ClockTimeStamp()
            if (today0Clock != Today0Clock) {
                Today0Clock = today0Clock
                Today4Clock = Today0Clock + 4 * MillisSecondOfHour
                Today22Clock = Today0Clock + 22 * MillisSecondOfHour
            }
            return lastUpdateTimeStamp <= 0 || now > Today4Clock && now < Today22Clock && now - lastUpdateTimeStamp > intervalHour * MillisSecondOfHour
        }

        fun nowAddRandomLong(): String? {
            val secureRandom = SecureRandom()
            val randomLong: Long = secureRandom.nextInt(100000).toLong()
            val timeStamp = System.currentTimeMillis()
            val timeStampString = timeStamp.toString()
            val randomString = randomLong.toString()
            return timeStampString + randomString
        }

        fun rand4SmsCode(): String {
            return (Random().nextInt(8999) + 1000).toString()
        }
    }

    fun get(id: Long): Triple<Boolean, String?, TYPE?> {// 指定 id ，返回单个资源对象
        TODO("wait for override") // 可选方法，等待被覆盖实现
    }

    fun get( // 指定多个条件，返回资源对象的列表
        parameterList: List<Pair<String, String>>? = null,      // 指定筛选条件
        groupByKeyList: List<String>? = null,           // 分组条件
        orderByList: List<String>? = null, orderByAscend: Boolean? = null,  // 排序条件
        limit: Int? = null,                             // 返回限定数量的记录
        offset: Int? = null,                            // 指定返回记录的开始位置
        pageIndex: Int? = null, perPage: Int? = null    // 指定第几页，以及每页限定的记录数量
    ): Triple<Boolean, String?, List<TYPE>?> {
        TODO("wait for override")
    }

    fun post(data: TYPE): Triple<Boolean, String?, TYPE?> { // 新建，返回新生成的资源对象
        TODO("wait for override")
    }

    fun put(data: TYPE): Triple<Boolean, String?, TYPE?> {  // 更新全部信息，返回完整的资源对象
        TODO("wait for override")
    }

    fun patch(data: TYPE): Triple<Boolean, String?, TYPE?> {// 更新部份信息，返回完整的资源对象
        TODO("wait for override")
    }

    fun delete(id: Long): Pair<Boolean, String?> { // 删除指定 id 的资源，返回成功与否
        TODO("wait for override")
    }

    fun delete(parameterMap: Map<String, String>): Triple<Boolean, String?, Int?> { // 删除指定条件的多个资源，返回成功删除资源对象的数量
        TODO("wait for override")
    }
}

/*  来自：[深入理解什么是RESTful API ？](https://www.jianshu.com/p/84568e364ee8)

GET用来获取资源，POST用来新建资源（也可以用于更新资源），PUT用来更新资源，DELETE用来删除资源。

GET     /zoos：列出所有动物园
POST    /zoos：新建一个动物园
GET     /zoos/ID：获取某个指定动物园的信息
PUT     /zoos/ID：更新某个指定动物园的信息（提供该动物园的全部信息）
PATCH   /zoos/ID：更新某个指定动物园的信息（提供该动物园的部分信息）
DELETE  /zoos/ID：删除某个动物园
GET     /zoos/ID/animals：列出某个指定动物园的所有动物
DELETE  /zoos/ID/animals/ID：删除某个指定动物园的指定动物

过滤信息（Filtering）
?limit=10：指定返回记录的数量
?offset=10：指定返回记录的开始位置。
?page=2&per_page=100：指定第几页，以及每页的记录数。
?sortby=name&order=asc：指定返回结果按照哪个属性排序，以及排序顺序。
?animal_type_id=1：指定筛选条件

返回结果
GET     /collection：返回资源对象的列表（数组）
GET     /collection/resource：返回单个资源对象
POST    /collection：返回新生成的资源对象
PUT     /collection/resource：返回完整的资源对象
PATCH   /collection/resource：返回完整的资源对象
DELETE  /collection/resource：返回一个空文档
*/