package com.booster

import android.content.Context

class GetRString(
    val context: Context
) {

    fun getHelloWorldString(): String {
        return context.getString(R.string.app_name)
    }

}