package com.booster.utility

import java.text.SimpleDateFormat
import java.util.*

/**
 * 来自：[github/maiconhellmann/DateExtension.kt](https://gist.github.com/maiconhellmann/796debb4007139d50e39f139be08811c)
 * 和 [Kotlin之“Date”扩展的一些高频率方法](https://www.jianshu.com/p/0a98a25f1b3a)
 * 和 [Kotlin入门(18)利用单例对象获取时间](https://blog.csdn.net/aqi00/article/details/82794535)
 */
fun Date.formatToString(format: String = "yyyy-MM-dd"): String {
    val simpleDateFormat = if (!format.isBlank()) SimpleDateFormat(format, Locale.getDefault()) else SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    return simpleDateFormat.format(this)
}

// 判断两个日期大小  如，第一个日期大于第二个日期，返回 true  反之false
fun Date.isDateOneBigger(dateStr1: String, dateStr2: String, format: String = ""): Boolean {
    val simpleDateFormat = if (!format.isBlank()) SimpleDateFormat(format, Locale.getDefault()) else SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
    val dateTime2 = simpleDateFormat.parse(dateStr2)?.time
    if (dateTime2 != null) {
        val result = simpleDateFormat.parse(dateStr1)?.time?.compareTo(dateTime2)
        // compareTo 函数 的结果不为空时 result == 0 说明一样大，小于 0 说明小于 dateTime2 ，大于 0 说明大于 dateTime2
        return if (result != null) result > 0 else false
    }
    return false
}