package com.booster.utility

import android.os.Parcelable
import com.booster.BuildConfig
import com.tencent.mmkv.MMKV
import com.tencent.mmkv.MMKVHandler
import com.tencent.mmkv.MMKVLogLevel
import com.tencent.mmkv.MMKVRecoverStrategic
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * 日志业务需要区别存储在 MMKV 里（id 为 "BOOSTER_MMKV_ID_LOG" + 日期如 20191218）
 */
@Parcelize  // [kotlin使用Parcelize注解简化Parcelable的书写](https://juejin.im/entry/5a261a8c6fb9a0450167cf1b)
data class MmkvLog(
    val tag: String,
    val level: MMKVLogLevel,
    val file: String,
    val func: String,
    val msg: String,
    // 有默认值的放后面
    val line: Int = 0, // 行数为 0 时表示不知是第几行代码
    val timeStamp: Long = System.currentTimeMillis()
) : Parcelable

inline fun saveMmkvLog(mmkvLog: () -> MmkvLog) { // 参考：[如何巧妙地规避不必要的开销](https://droidyue.com/blog/2019/11/24/smart-log-in-android-slash-kotlin/)
    val log = mmkvLog()
    val mmkv: MMKV? = MmkvLogUtil.mmkv
    if (mmkv == null || !mmkv.encode(
            "${log.timeStamp}",
            log
        )
    ) { // 保存进 MMKV  // 从 MMKV 取出来 mmkv.decodeParcelable("log", MmkvLog::class.java)
        println("MMKV_LOG_UTIL 出错：保存失败：${log}")
    }

    if (BuildConfig.DEBUG) { // 是 debug 版本，才打印出来看看
        val tag = "MMKV_Handler_日志"
        val line = if (log.line <= 0) "line_zero" else "line${log.line}"
        val logString = "<${log.file} --> $line <--> ${log.func} : ${log.msg}"
        when (log.level) {
            MMKVLogLevel.LevelDebug -> {
                println("$tag debug: $logString")
            }
            MMKVLogLevel.LevelInfo -> {
                println("$tag info: $logString")
            }
            MMKVLogLevel.LevelWarning -> {
                println("$tag warning: $logString")
            }
            MMKVLogLevel.LevelError -> {
                println("$tag 出错：\$logString")
            }
            MMKVLogLevel.LevelNone -> { // MMKVLogLevel.LevelNone 是关闭了 MMKV 的日志输出
                println("$tag 停止后：\$logString")
            }
        }
    }
}

object MmkvLogUtil : MMKVHandler {
    var isInTestStatus = false // 处于测试状态，返回 mmkv 为空，默认不处于测试状态
    // 懒加载 val 属性，查看[Kotlin —  lateinit vs lazy](https://www.jianshu.com/p/fa4fa622694f)
    // by lazy 是第一次被调用 get 这个变量时才去赋值，下次再来就用这个值了
    // 需要多进程访问 MMKV ，在初始化的时候加上标志位 MMKV.MULTI_PROCESS_MODE：

    val mmkv: MMKV? by lazy {
        if (isInTestStatus) MMKV.mmkvWithID(
            "BOOSTER_MMKV_ID_LOG" + Date().formatToString(),
            MMKV.MULTI_PROCESS_MODE
        ) else null
    }

    // --- 以下实现 MMKVHandler 的接口
    override fun wantLogRedirecting(): Boolean {
        return true // true 表示日志传给这里处理
    }

    override fun mmkvLog(
        level: MMKVLogLevel,
        file: String,
        line: Int,
        func: String,
        msg: String
    ) {
        saveMmkvLog {
            MmkvLog("MMKV_Handler_日志", level, file, func, msg, line)
        }
    }

    override fun onMMKVFileLengthError(p0: String?): MMKVRecoverStrategic {
        val tag = "MMKV_Handler_日志<出错>"
        if (p0 != null && p0.isNullOrBlank()) println("$tag 出错：$p0") else println("$tag 出错：但没有错误信息")
        return MMKVRecoverStrategic.OnErrorRecover // 此句来自[官方 github 页面](https://github.com/Tencent/MMKV/wiki/android_advance_cn)
    }

    override fun onMMKVCRCCheckFail(p0: String?): MMKVRecoverStrategic {
        val tag = "MMKV_Handler_日志<出错>"
        if (p0 != null && p0.isNullOrBlank()) println("$tag 出错：$p0") else println("$tag 出错：但没有错误信息")
        return MMKVRecoverStrategic.OnErrorDiscard // 此句来自[官方 github 页面](https://github.com/Tencent/MMKV/wiki/android_advance_cn)
    }
}