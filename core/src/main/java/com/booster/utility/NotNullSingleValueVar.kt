package com.booster.utility

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * 单例化的第三种方式：自定义一个非空且只能一次性赋值的委托属性，
 * 来自[Kotlin入门(28)Application单例化](https://blog.csdn.net/aqi00/article/details/83310069)
 */
// 定义一个属性管理类，进行非空和重复赋值的判断
class NotNullSingleValueVar<T>() : ReadWriteProperty<Any?, T> {
    private var value: T? = null

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value ?: throw IllegalStateException("application not initialized")
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = if (this.value == null) value
        else throw IllegalStateException("application already initialized")
    }
}