package com.booster.utility

import android.text.TextUtils
import android.util.Base64
import com.tencent.mmkv.MMKVLogLevel
import java.security.Key
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

private const val TAG = "EasyAES"

/**
 * AES 加解密，对应有测试通过的 php 代码和 未测试过的 iOS 代码
 *
 * @author pkuoliver
 * 来源 2019-09-06  https://github.com/pkuoliver/EasyAES/blob/master/Android/EasyAES.java
 * @see Base64 *

 * 使用128 Bits或是256 Bits的AES秘钥(计算任意长度秘钥的MD5或是SHA256)，用MD5计算IV值
 * @param keyString 传入任意长度的AES秘钥
 * @param bit 传入AES秘钥长度，数值可以是128、256 (Bits)
 * @param ivString  传入任意长度的IV字串
 */
class EasyAES constructor(
    keyString: String,
    bit: Int,
    ivString: String
) {
    // 取得AES加解密的秘钥
    private var key: Key? = null
    // AES CBC模式使用的Initialization Vector
    private var iv: IvParameterSpec? = null
    // Cipher 物件
    private var cipher: Cipher? = null

    // 构造函数，使用128 Bits的AES秘钥(计算任意长度秘钥的MD5)和预设IV
    init {
        if (bit == 256) {
            this.key = SecretKeySpec(getHash("SHA-256", keyString), ALGORITHM) as Key?
        } else {
            this.key = SecretKeySpec(getHash("MD5", keyString), ALGORITHM)
        }
        if (ivString.isNotBlank()) {
            this.iv = IvParameterSpec(getHash("MD5", ivString))
        } else {
            this.iv = DEFAULT_IV
        }
        initCipher()
    }

    // 初始化加密器
    private fun initCipher() {
        cipher = try {
            Cipher.getInstance(TRANSFORMATION)
        } catch (exception: Exception) {
            saveMmkvLog {
                val func = "initCipher"
                MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
            }
            exception.printStackTrace()
            throw RuntimeException(exception.message)
        }
    }

    // 加密文字
    private fun encrypt(str: String): String {
        return try {
            encrypt(str.toByteArray(charset("UTF-8")))
        } catch (exception: Exception) {
            saveMmkvLog {
                val func = "encrypt(String)"
                MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
            }
            exception.printStackTrace()
            throw RuntimeException(exception.message)
        }
    }

    // 加密字节数组，返回字符串
    private fun encrypt(data: ByteArray?): String {
        return try {
            cipher!!.init(Cipher.ENCRYPT_MODE, key, iv)
            val encryptData = cipher!!.doFinal(data)
            String(Base64.encode(encryptData, Base64.DEFAULT), Charsets.UTF_8)
        } catch (exception: Exception) {
            saveMmkvLog {
                val func = "encrypt(ByteArray)"
                MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
            }
            exception.printStackTrace()
            throw RuntimeException(exception.message)
        }
    }

    /**
     * 解密文字
     *
     * @param str 传入要解密的文字
     * @return 传回解密后的文字
     */
    private fun decrypt(str: String?): String {
        return try {
            decrypt(Base64.decode(str, Base64.DEFAULT))
        } catch (exception: Exception) {
            saveMmkvLog {
                val func = "decrypt(String)"
                MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
            }
            exception.printStackTrace()
            throw RuntimeException(exception.message)
        }
    }

    /**
     * 解密文字
     *
     * @param data 传入要解密的资料
     * @return 传回解密后的文字
     */
    private fun decrypt(data: ByteArray?): String {
        return try {
            cipher!!.init(Cipher.DECRYPT_MODE, key, iv)
            val decryptData = cipher!!.doFinal(data)
            String(decryptData, Charsets.UTF_8)
        } catch (exception: Exception) {
            saveMmkvLog {
                val func = "decrypt(ByteArray)"
                MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
            }
            exception.printStackTrace()
            throw RuntimeException(exception.message)
        }
    }

    companion object {
        //-----类常量-----
        /**
         * 预设的Initialization Vector，为16 Bits的0
         */
        private val DEFAULT_IV =
            IvParameterSpec(byteArrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
        /**
         * 加密演算法使用AES
         */
        private const val ALGORITHM = "AES"
        /**
         * AES使用CBC模式与PKCS5Padding
         */
        private const val TRANSFORMATION = "AES/CBC/PKCS5Padding"
        //-----物件方法-----
        /**
         * 取得字串的Hash值
         *
         * @param algorithm 传入散列算法
         * @param text      传入要散列的字串
         * @return 传回散列后內容
         */
        private fun getHash(algorithm: String, text: String): ByteArray {
            return try {
                getHash(algorithm, text.toByteArray(charset("UTF-8")))
            } catch (exception: Exception) {
                saveMmkvLog {
                    val func = "getHash(String, String)"
                    MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
                }
                exception.printStackTrace()
                throw RuntimeException(exception.message)
            }
        }

        /**
         * 取得资料的Hash值
         *
         * @param algorithm 传入散列算法
         * @param data      传入要散列的资料
         * @return 传回散列后內容
         */
        private fun getHash(algorithm: String, data: ByteArray): ByteArray {
            return try {
                val digest = MessageDigest.getInstance(algorithm)
                digest.update(data)
                digest.digest()
            } catch (exception: Exception) {
                saveMmkvLog {
                    val func = "getHash(String, ByteArray)"
                    MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
                }
                exception.printStackTrace()
                throw RuntimeException(exception.message)
            }
        }

        private const val keyString = "nv9qnyu48d2yi0qzo1803tb1tuzclc3t" // EasyAes key
        private const val ivString = "dobv6kirx758br8j9ic5vc6ed8tan12u" // EasyAes iv
        fun encryptString256(content: String): String { //这里填写密码和iv字符串，注意要确保16位的
            val ea = EasyAES(keyString, 256, ivString)
            return ea.encrypt(content)
        }

        fun decryptString256(content: String?): String? {
            var result: String? = null
            if (!TextUtils.isEmpty(content)) {
                try { //这里填写密码和iv字符串，注意要确保16位的
                    val ea = EasyAES(keyString, 256, ivString)
                    result = ea.decrypt(content)
                } catch (exception: Exception) {
                    saveMmkvLog {
                        val func = "decryptString256(String)"
                        MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, exception.toString())
                    }
                    exception.printStackTrace()
                    throw RuntimeException(exception.message)
                }
            }
            return result
        }
    }
}