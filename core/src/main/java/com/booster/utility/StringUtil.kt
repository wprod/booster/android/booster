package com.booster.utility

import java.io.UnsupportedEncodingException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * 将字符串转成 MD5 值
 */
fun String.md5(): String? {
    val hash: ByteArray
    hash = try {
        MessageDigest.getInstance("MD5").digest(toByteArray(charset("UTF-8")))
    } catch (exception: NoSuchAlgorithmException) {
        exception.printStackTrace()
        return null
    } catch (exception: UnsupportedEncodingException) {
        exception.printStackTrace()
        return null
    }
    val base = 0xFF
    val hex = StringBuilder(hash.size * 2)
    for (b in hash) {
        val bValue = b.toInt()
        if (base.and(bValue) < 0x10) {
            hex.append("0")
        }
        hex.append(Integer.toHexString(base.and(bValue)))
    }
    return hex.toString()
}