package com.booster.data.remote

import com.booster.StaticApp
import com.booster.device.DeviceInfo
import com.booster.domain.entity.UserEntity
import com.booster.domain.entity.ValidityEntity
import com.booster.domain.repository.Repository
import com.booster.utility.EasyAES
import com.booster.utility.MmkvLog
import com.booster.utility.formatToString
import com.booster.utility.saveMmkvLog
import com.github.kittinunf.fuel.Fuel
import com.tencent.mmkv.MMKVLogLevel
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import java.util.*

private const val TAG = "UserRemoteData"

@Serializable
private data class PromoterResult(
    val success: String,
    val msg: String?,
    val userid: Long?,
    val mobile: String?
)

@Serializable
private data class RegisterResult constructor(
    val olduser: String,        // true:老用户   false:新用户
    val success: String,        // true:注册成功  false:查看msg字段的错误原因
    val userid: Long?,          // 当success=true或Olduser=true时，返回用户id
    val username: String?,      // 当success=true或Olduser=true时，返回用户名
    val showpsw: String?,       // 当success=true或Olduser=true时，返回明文密码
    val psw: String?,           // 当success=true或Olduser=true时，返回加密密码
    val todaylogin: Int?,       // 当success=true或Olduser=true时，今天登录了几台手机，多于 1 台会提示用户
    val validtime: Long?,       // 当success=true或Olduser=true时，过期时刻，接口给的是 10 位数，乘 1000 后保存
    val viptext: String?,       // 当success=true或Olduser=true时，有效期显示用的描述
    val msg: String?            // 当success=false时有这个值
)

@Serializable
private data class UserInfo(
    val is_disabled: String
)

@Serializable
private data class UserData(
    val days: Double,
    val id: String,
    val psw: String,
    val todaylogin: Int,
    val username: String
)

@Serializable
private data class LogInResult(
    val success: String, // 登录失败时 {"success":"undefined","msg":"1003 密码错误！"}
    val msg: String?,
    val charge_promotion_tip: String?,
    val info: UserInfo?,
    val session_id: String?,
    val user: UserData?
)

@Serializable
private data class PatchResult constructor(
    val success: String,
    val msg: String?,
    val userid: Long?,
    val username: String?,
    val psw: String?,   // PatchPwdBySmsCode 返回的是 psw
    val pwd: String?    // PatchPwdById      返回的是 pwd
)

class UserRemoteData(var smsCode: String? = null, var newPwd: String? = null) : Repository<UserEntity> {
    // 外界主动要求生成一个随机的短信码，生成后保留，调用改密码接口时用
    fun randomSmsCode(): String {
        smsCode = Repository.rand4SmsCode()
        return smsCode!!
    }

    fun clearSmsCode() {
        smsCode = null;
    }

    // 免注册登录前，获取推荐人账号的接口
    private fun promoter(): PromoterResult? {
        val urlString =
            "http://www.hongxing.monster/index.php?g=user&m=api&a=getShareid&appname=${Repository.SHORT_APP_NAME}"
        val (_, _, result) = Fuel.get(urlString).responseString()
        return result.fold(
            { newData ->
                val json = Json(JsonConfiguration.Stable)
                val promoterResult = json.parse(PromoterResult.serializer(), newData)
                if ("true" != promoterResult.success) {
                    saveMmkvLog {
                        val func = "promoter()"
                        val msg = "获取推荐人失败：${promoterResult.msg}"
                        MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
                    }
                }
                promoterResult
            },
            { error ->
                val errorString = error.toString()
                saveMmkvLog {
                    val func = "promoter()"
                    MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, errorString)
                }
                null
            }
        )
    }

    // 免注册登录接口
    private fun register(): Triple<Boolean, String?, List<UserEntity>?> {
        val appName = Repository.SHORT_APP_NAME
        val deviceId = DeviceInfo.uuid
        val parameterList = mutableListOf(
            "appname" to appName,
            "devid" to deviceId,
            "channel" to StaticApp.instance().channel,
            "sign" to EasyAES.encryptString256("$appName$deviceId")
        )
        val decryptSign = EasyAES.decryptString256(parameterList[3].second)
        println(decryptSign) // TODO 测试一下是否解密成功
        // 获取推荐人账号
        val promoterResult = promoter()
        if (promoterResult?.mobile != null && promoterResult.mobile.isNotBlank()) {
            parameterList.add("sharemobile" to promoterResult.mobile) // 如果有就加入到参数中
        }
        val urlString = "http://www.hongxing.monster/index.php?g=user&m=register&a=devreg"
        val (_, _, result) = Fuel.post(
            urlString,
            parameterList
        ).responseString()
        return result.fold(
            { newData ->
                val json = Json(JsonConfiguration.Stable)
                val registerResult = json.parse(RegisterResult.serializer(), newData)
                if ("true" == registerResult.olduser || "true" == registerResult.success) {
                    val user = UserEntity(
                        id = registerResult.userid,
                        inputPwd = registerResult.showpsw,    // 明文密码
                        nameFromServer = registerResult.username,
                        pwdFromServer = registerResult.psw,
                        validity = ValidityEntity(
                            deviceSignInToday = registerResult.todaylogin,
                            expiry = registerResult.validtime,
                            desc = registerResult.viptext
                        ),
                        logInTime = System.currentTimeMillis()
                    )
                    Triple(true, null, listOf(user))
                } else {
                    saveMmkvLog {
                        val func = "register()"
                        val msg = "免注册登录失败：${registerResult.msg}"
                        MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
                    }
                    Triple(false, registerResult.msg, null)
                }
            },
            { error ->
                val errorString = error.toString()
                saveMmkvLog {
                    val func = "register()"
                    MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, errorString)
                }
                Triple(false, errorString, null)
            }
        )
    }

    private fun logIn(
        name: String,
        pwd: String,
        isOnceADay: Boolean
    ): Triple<Boolean, String?, List<UserEntity>?> {
        val deviceId = DeviceInfo.uuid
        val parameterList = mutableListOf(
            "username" to name,
            "password" to pwd,
            "devid" to deviceId,
            "channel" to StaticApp.instance().channel
        )
        if (isOnceADay) { // 日活登录需要添加日期参数
            parameterList.add("login_day" to Date().formatToString("yyyyMMdd"))
        }
        val urlString =
            "http://www.hongxing.monster/index.php?g=user&m=register&a=devreg&appname=${Repository.SHORT_APP_NAME}"
        val (_, _, result) = Fuel.post(
            urlString,
            parameterList
        ).responseString()
        return result.fold(
            { newData ->
                val json = Json(JsonConfiguration.Stable)
                val logInResult = json.parse(LogInResult.serializer(), newData)
                if (logInResult.success == "true") {
                    val now = System.currentTimeMillis()
                    // 天数变成时间戳，如 10.902 天 = 10.902 x 24小时 x 60分钟 x 60秒钟 x 1000 毫秒 + 当前时间戳
                    val expiry =
                        if (logInResult.user == null) null else (logInResult.user.days * 24 * 60 * 60 * 1000).toLong() + now
                    val user = UserEntity(
                        id = logInResult.user?.id?.toLong(),
                        inputPwd = pwd, // 登录用的密码都是明文密码
                        nameFromServer = logInResult.user?.username,    // 服务器返回的用户名
                        pwdFromServer = logInResult.user?.psw,          // 服务器返回的密码
                        validity = ValidityEntity(
                            deviceSignInToday = logInResult.user?.todaylogin,
                            expiry = expiry,
                            desc = null
                        ),
                        logInTime = now
                    )
                    if (!isOnceADay) { // 如果是账号密码登录，这用户名就是用户输入的用户名
                        user.inputName = name
                    }
                    Triple(true, null, listOf(user))
                } else {
                    saveMmkvLog {
                        val func = "logIn($parameterList, $isOnceADay)"
                        val msg = "${if (isOnceADay) "日活" else "账号密码"}登录失败：${logInResult.msg}"
                        MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
                    }
                    Triple(false, logInResult.msg, null)
                }
            },
            { error ->
                val errorString = error.toString()
                saveMmkvLog {
                    val func = "logIn($parameterList, $isOnceADay)"
                    val msg = "${if (isOnceADay) "日活" else "账号密码"}登录失败：${errorString}"
                    MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, errorString)
                }
                Triple(false, errorString, null)
            }
        )
    }

    private fun patchPwdBySmsCode(name: String): Triple<Boolean, String?, PatchResult?> {
        return if (smsCode.isNullOrBlank() || name.isBlank() || newPwd.isNullOrBlank()) {
            val msg = "参数为空值，无法执行"
            saveMmkvLog {
                val func = "patchPwdBySmsCode($name, $newPwd)"
                MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
            }
            Triple(false, msg, null)
        } else {
            val parameterList = listOf(
                "email" to name,
                "scode" to smsCode,
                "password" to newPwd,
                "mobilereg" to "1", // jiuai 原本设计就是 mobilereg 固定为 1，20190831 不做修改
                "app" to Repository.nowAddRandomLong().toString(),
                "smsreg" to "true"
            )
            val urlString =
                "http://www.hongxing.monster/index.php?g=user&m=register&a=password_change_crx&appname=${Repository.SHORT_APP_NAME}"
            val (_, _, result) = Fuel.post(
                urlString,
                parameterList
            ).responseString()
            result.fold(
                { newData ->
                    val json = Json(JsonConfiguration.Stable)
                    return Triple(true, null, json.parse(PatchResult.serializer(), newData))
                },
                { error ->
                    saveMmkvLog {
                        val func = "patchPwdBySmsCode($name, $newPwd)"
                        MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, error.toString())
                    }
                    Triple(false, error.toString(), null)
                }
            )
        }
    }

    private fun patchPwdById(id: Long, pwd: String): Triple<Boolean, String?, PatchResult?> {
        return if (pwd.isBlank() || newPwd.isNullOrBlank()) {
            val msg = "参数为空值，无法执行"
            saveMmkvLog {
                val func = "patchPwdById($id, $pwd, $newPwd)"
                MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
            }
            Triple(false, msg, null)
        } else {
            val parameterList = listOf(
                "uid" to id,
                "pwd" to pwd,
                "npwd" to newPwd
            )
            val urlString =
                "http://www.hongxing.monster/index.php?g=user&m=register&a=password_change_crx&appname=${Repository.SHORT_APP_NAME}"
            val (_, _, result) = Fuel.post(
                urlString,
                parameterList
            ).responseString()
            result.fold(
                { newData ->
                    val json = Json(JsonConfiguration.Stable)
                    return Triple(true, null, json.parse(PatchResult.serializer(), newData))
                },
                { error ->
                    saveMmkvLog {
                        val func = "patchPwdById($id, $pwd, $newPwd)"
                        MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, error.toString())
                    }
                    Triple(false, error.toString(), null)
                }
            )
        }
    }

    // 看最下面的 # 2019-12-20 免注册接口
    override fun get( // 指定多个条件，返回资源对象的列表
        parameterList: List<Pair<String, String>>?,      // 指定筛选条件
        groupByKeyList: List<String>?,           // 分组条件
        orderByList: List<String>?, orderByAscend: Boolean?,  // 排序条件
        limit: Int?,                             // 返回限定数量的记录
        offset: Int?,                            // 指定返回记录的开始位置
        pageIndex: Int?, perPage: Int?    // 指定第几页，以及每页限定的记录数量
    ): Triple<Boolean, String?, List<UserEntity>?> {
        return if (parameterList == null || parameterList.isEmpty()) { // 没有参数，就是免注册登录
            register()
        } else { // 有参数，就是日活登录，parameterList 里有 inputName 或 nameFromServer, inputPwd, 或许有 login_day
            // 参数两个，说明是账号密码登录，如果是多于两个，说明带着的是服务器返回的账号，则代表是日活登录
            var name: String? = null
            var pwd: String? = null
            var isLogInOnceADay = false
            for ((key, value) in parameterList) {
                if ("name" == key) {
                    name = value
                } else if ("pwd" == key) {
                    pwd = value
                } else if ("nameFromServer" == key) {
                    isLogInOnceADay = value == "true"
                }
            }
            if (name.isNullOrBlank() || pwd.isNullOrBlank()) {
                val msg = "参数为空值，无法执行"
                saveMmkvLog {
                    val func = "get($parameterList)"
                    MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
                }
                Triple(false, msg, null)
            } else {
                logIn(name, pwd, isLogInOnceADay)
            }
        }
    }

    // 修改密码可用 短信验证码 或 id
    override fun patch(data: UserEntity): Triple<Boolean, String?, UserEntity?> {
        val id = data.id
        val inputPwd = data.inputPwd
        val inputName = data.inputName
        return if (smsCode.isNullOrBlank() && id != null && !inputPwd.isNullOrBlank()) {
            val (ok, msg, patchResult) = patchPwdById(id, inputPwd)
            if (ok && patchResult != null && !patchResult.pwd.isNullOrBlank()) {
                data.inputPwd = newPwd
                data.pwdFromServer = patchResult.pwd
                Triple(true, null, data)
            } else Triple(ok, msg, data)
        } else if (!inputName.isNullOrBlank()) {
            val (ok, msg, patchResult) = patchPwdBySmsCode(inputName)
            if (ok && patchResult != null && !patchResult.pwd.isNullOrBlank()) {
                data.inputPwd = newPwd
                data.pwdFromServer = patchResult.pwd
                Triple(true, null, data)
            } else Triple(ok, msg, data)
        } else {
            val msg = "参数为空值，无法执行"
            saveMmkvLog {
                val func = "patch($data)"
                MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
            }
            Triple(false, msg, null)
        }
    }
}

/*
# 2019-12-20 免注册接口

注册域名拼接 + index.php?g=user&m=register&a=devreg

POST参数（全小写）：
1.Appname   AppName（ffo,hxo,lho）
2.Devid  	设备ID
3.Channel 	渠道号
4.Sign    	加密 Appname + Devid
5.sharemobile 如果有推荐人账号，就带上，没有就不带

返回JSON（全小写）：
olduser    	true: 老用户   false:新用户
success    	true:注册成功  false:查看msg字段的错误原因
userid  	当success=true或Olduser=true时，返回用户id
username 	当success=true或Olduser=true时，返回用户名
psw  		当success=true或Olduser=true时，返回加密密码
showpsw 	当success=true或Olduser=true时，返回明文密码
validtime   过期时刻，接口给的是 10 位数，乘 1000 保存起来
viptext     显示用的有效期文字，暂时定为小于 1 天时使用

*/

/* 账号密码登录和日活登录接口

返回JSON： 登录成功时：
{"success":true,"charge_promotion_tip":"",
 "user":{"days":10.902,"id":"214475","username":"C12511111111","psw":"94811bec9fb5c2acdf881b1b84165f37","todaylogin":0},
 "info":{"is_disabled":"false"},
 "session_id":"0000"
}

登录失败时：
{"success":"undefined","msg":"1003 密码错误！"}
 */