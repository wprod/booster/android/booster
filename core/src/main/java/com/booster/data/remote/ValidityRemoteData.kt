package com.booster.data.remote

import com.booster.device.DeviceInfo
import com.booster.domain.entity.ValidityEntity
import com.booster.domain.repository.Repository
import com.booster.utility.MmkvLog
import com.booster.utility.saveMmkvLog
import com.github.kittinunf.fuel.Fuel
import com.tencent.mmkv.MMKVLogLevel
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

private const val TAG = "ValidityRemoteData"

@Serializable
data class ValidityResult(
    val isexpiry: Boolean,  // 是否过期
    val todaylogin: Int,    // 今天登录了几台手机，多于 1 台会提示用户
    val validtime: Long,    // TODO 接口给的是 10 位数，乘 1000 保存起来
    val viptext: String     // 服务器给的显示有效期的文字
)

class ValidityRemoteData : Repository<ValidityEntity> {
    fun convertToValidityEntity(validityResult: ValidityResult): ValidityEntity {
        return ValidityEntity(
            validityResult.todaylogin,
            validityResult.validtime,
            validityResult.viptext
        )
    }

    override fun get(id: Long): Triple<Boolean, String?, ValidityEntity?> {
        val urlString =
            "http://www.hongxing.monster/index.php?g=user&m=api&a=getUserValtime&appname=${Repository.SHORT_APP_NAME}"
        val (_, _, result) = Fuel.post(
            urlString,
            listOf("uid" to id, "deviceId" to DeviceInfo.uuid)
        ).responseString()
        return result.fold(
            { data ->
                val json = Json(JsonConfiguration.Stable)
                val validityResult = json.parse(ValidityResult.serializer(), data)
                Triple(false, null, convertToValidityEntity(validityResult))
            },
            { error ->
                saveMmkvLog {
                    val func = "queryById"
                    val msg = "queryById，返回失败"
                    MmkvLog(TAG, MMKVLogLevel.LevelWarning, TAG, func, msg)
                }
                Triple(false, error.toString(), null)
            }
        )
    }
}