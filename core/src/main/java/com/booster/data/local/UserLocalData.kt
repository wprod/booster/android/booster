package com.booster.data.local

import com.booster.domain.entity.UserEntity
import com.booster.domain.repository.Repository
import com.booster.utility.MmkvLog
import com.booster.utility.saveMmkvLog
import com.tencent.mmkv.MMKV
import com.tencent.mmkv.MMKVLogLevel

private const val TAG = "UserLocalData"

class UserLocalData : Repository<UserEntity> {
    private val mmkvDefault: MMKV by lazy { MMKV.defaultMMKV() }

    override fun put(data: UserEntity): Triple<Boolean, String?, UserEntity?> {
        return if (mmkvDefault.encode(TAG, data)) {
            Triple(true, null, data)
        } else {
            saveMmkvLog {
                val func = "put($data)"
                val msg = "mmkv.encode($TAG, $data)，返回失败"
                MmkvLog(TAG, MMKVLogLevel.LevelError, TAG, func, msg)
            }
            Triple(false, "保存失败", data)
        }
    }

    override fun delete(id: Long): Pair<Boolean, String?> {
        mmkvDefault.removeValueForKey(TAG)
        return Pair(true, null)
    }

    override fun get( // 指定多个条件，返回资源对象的列表
        parameterList: List<Pair<String, String>>?,      // 指定筛选条件
        groupByKeyList: List<String>?,           // 分组条件
        orderByList: List<String>?, orderByAscend: Boolean?,  // 排序条件
        limit: Int?,                             // 返回限定数量的记录
        offset: Int?,                            // 指定返回记录的开始位置
        pageIndex: Int?, perPage: Int?    // 指定第几页，以及每页限定的记录数量
    ): Triple<Boolean, String?, List<UserEntity>?> {
        val user = mmkvDefault.decodeParcelable(TAG, UserEntity::class.java)
        return if (user == null) {
            Triple(false, "当前没有用户", null)
        } else {
            Triple(true, null, listOf(user))
        }
    }
}