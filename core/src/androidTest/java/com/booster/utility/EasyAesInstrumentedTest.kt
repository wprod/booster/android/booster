package com.booster.utility

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class EasyAesInstrumentedTest {
    @Test
    fun testEncrypt() {
        // 使用 assertEquals
        var content = "..我来测试11$$\n"
        var encryptContent = "Cw+iU9l6AEt8cJx9Q5RD53WTx3DSpBcSmaagZgEDTdY=\n"
        var encryptResult = EasyAES.encryptString256(content)
        var decryptResult = EasyAES.decryptString256(encryptResult)
        Assert.assertEquals("EasyAes 加密出错", encryptContent, encryptResult)
        Assert.assertEquals("EasyAes 解密出错", content, decryptResult)

        content = "我爱浩崽崽"
        encryptContent = "egf+jq8zxTJQzm6PmBu0PQ==\n"
        encryptResult = EasyAES.encryptString256(content)
        decryptResult = EasyAES.decryptString256(encryptResult)
        Assert.assertEquals("EasyAes 加密出错", encryptContent, encryptResult)
        Assert.assertEquals("EasyAes 解密出错", content, decryptResult)
    }
}