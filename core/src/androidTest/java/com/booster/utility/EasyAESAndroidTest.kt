package com.booster.utility

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertEquals

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class EasyAESAndroidTest {
    @Test
    fun testEncrypt() {
        // 使用 assertEquals
        var content = "..我来测试\n1English1$$" // 尝试字符串里包括中文，英文字符，特殊字符，换行符
        var encryptContent = "ZDzE+EQiqx2sDIEP4lvT4fT8ZAmgFKh7L66ud4ez2vQ=\n" // 加密出来的字符串总是会在最后面加上 \n ，不知为何
        var encryptResult = EasyAES.encryptString256(content)
        var decryptResult = EasyAES.decryptString256(encryptResult)
        assertEquals(encryptContent, encryptResult, "EasyAes 加密出错")
        assertEquals(content, decryptResult, "EasyAes 解密出错")

        content = "我爱浩崽崽"
        encryptContent = "egf+jq8zxTJQzm6PmBu0PQ==\n"
        encryptResult = EasyAES.encryptString256(content)
        decryptResult = EasyAES.decryptString256(encryptResult)
        assertEquals(encryptContent, encryptResult, "EasyAes 加密出错")
        assertEquals(content, decryptResult, "EasyAes 解密出错")
    }
}