# 2019-12-17 15:21
1. 用 Android Studio 4.0 preview 6 版本开启了 Booster 的开发，用 Kotlin
2. 选用 Clean Architecture ，先实现 Domain
3. Domain 的 Entity 直接用 data class Xxx(val yyy: String, var zzz: Int) ，参考：[Kotlin 数据类](https://www.kotlincn.net/docs/reference/data-classes.html)
4. 接口及其可选方法的声明  
```
interface MyInterface {
    fun bar()
    fun foo() {
      // 可选的方法体
    }
}
```
5. Swift 函数可以返回多个值，Kotlin的返回 2 个可以用 Pair<Type1, Type2> ，3 个可用Triple<Type1, Type2, Type3> ，4 个或 5 个相同类型的可以用 Array<Type>,  
   再用解构方式获取，超过 5 个的相同类型，或者 超过 3 个的不同类型，就要用 data class 方式来实现，  
   参考[我们如何像 Swfit 一样从 Kotlin 中的函数返回多个值？ ](https://stackoom.com/question/3CUuk/%E6%88%91%E4%BB%AC%E5%A6%82%E4%BD%95%E5%83%8F%E8%BF%85%E9%80%9F%E4%B8%80%E6%A0%B7%E4%BB%8EKotlin%E4%B8%AD%E7%9A%84%E5%87%BD%E6%95%B0%E8%BF%94%E5%9B%9E%E5%A4%9A%E4%B8%AA%E5%80%BC)  
6. 如果生成的类需要含有一个无参的构造函数，则所有的属性必须指定默认值。   
   data class User(val name: String = "", var age: Int = 0)
7. Kotlin 类中的属性既可以用关键字 var 声明为可变的，也可以用关键字 val 声明为只读的。  
8. 如果是一个可为空的变量，用 if (xxx != null) {} 判断了再用，使用就不带 ?
```
    val current = currentUser()
    if (current != null) {
        current.validity = validity
        repository.save(current)
    }}
``` 

# 2019-12-18 09:48
1. 用 object 声明一个单例，查看 [[译]Object的局限性——Kotlin中的带参单例模式](https://blog.csdn.net/mq2553299/article/details/87128674)
2. 懒加载 val 属性，查看[Kotlin —  lateinit vs lazy](https://www.jianshu.com/p/fa4fa622694f)
3. import SimpleDateFormat 时有个选择，是 java.text.SimpleDateFormat 还是 android.icu.text.SimpleDateFormat  
   答案是 java.text.SimpleDateFormat ，因为后者只支持 API 24 以上，以下使用会崩溃
4. 工具类如日期工具，用单例还是扩展？答案是用扩展，参考：[Android使用Kotlin开发工具类，应该使用object还是companion object？](https://www.zhihu.com/question/264059532)。  
   因为用扩展反编译结果，就是 java 里的  static 方法
5. 日期时间工具类，可以参看：  
   * [github/maiconhellmann/DateExtension.kt](https://gist.github.com/maiconhellmann/796debb4007139d50e39f139be08811c)
   * 和 [Kotlin之“Date”扩展的一些高频率方法](https://www.jianshu.com/p/0a98a25f1b3a)
   * 和 [Kotlin入门(18)利用单例对象获取时间](https://blog.csdn.net/aqi00/article/details/82794535)
6. 调用打印日志的函数一般需要先拼接出字符串，里面再判断是否 debug 版本，是才打印出来看，如果不是，这字符串就白白浪费了，  
   kotlin 里如何避免，参考：[如何巧妙地规避不必要的开销](https://droidyue.com/blog/2019/11/24/smart-log-in-android-slash-kotlin/)
```
相对最完美的版本
这个版本是相对最好的实现，规避了非Debug环境下的字符串拼接和具体求值的操作

inline fun smartMessage(lazyMessage: () -> Any?) {
    if (BuildConfig.DEBUG) {
        Log.d("smartMessage", lazyMessage().toString())
    }
}
private fun testSmartMessage() {
    smartMessage {
        "getProperties " + getProperties()
    }
}

上面使用了Lambda表达式来生成message信息
如何巧妙地规避不必要的开销

当我们反编译Kotlin 代码 到 Java代码时，一切就清晰了。

private final void testSmartMessage() {
      int $i$f$smartMessage = false;
      if (BuildConfig.DEBUG) {
         String var3 = "smartMessage";
         int var2 = false;
         String var4 = "getProperties " + this.getProperties();
         Log.d(var3, String.valueOf(var4));
      }
}

之前的Lambda 由于采用了 inline 处理 会把smartMessage 提取到调用处testSmartMessage
上面的信息，都是确保了在BuildConfig.DEBUG成立时才执行，否则不执行
上面的做法，利用了Kotlin的特性，就运行时可能存在的开销一下就移除了。
```
7. MMKV 保存自定义的对象，对象需要实现 Parcelable 的接口，很简单，  
   看：[kotlin使用Parcelize注解简化Parcelable的书写](https://juejin.im/entry/5a261a8c6fb9a0450167cf1b)  
   2019-12-21 下午15点补充：  
   记得要在 app 的 build.gradle 里应用 kotlin 扩展，添加这一句：apply plugin: 'kotlin-android-extensions'  
   否则编译时会报错：
   ```
   Class X is not abstract and does not implement fun writeToParcel() defined in android.os.Parcelable
   ```
   
8. 用 MMKV 保存实现了 Parcelable 接口的实例 parcelableImpl ：MMKV.defaultMMKV().encode("key", parcelableImpl)  
   取出类型为 ParcelableImpl 的数据变成实例：                MMKV.defaultMMKV().decodeParcelable("key", ParcelableImpl::class.java)
9. 对集合类的操作，参考 [Kotlin 函数式编程三板斧](http://ohmerhe.com/2016/07/05/kotlin_function_three_common_methods/)  
   针对 Map 类型数据集合，提供了 filterKeys 和 filterValues 方法，方便只做 key 或者 value 的判断。  
   看：[Kotlin——高级篇（五）：集合之常用操作符汇总](https://juejin.im/post/5b1f7699f265da6e155d5965)
```
val params = mapOf("limit" to "10", "offset" to "20", "order" to "asc")
val s = params.map { "${it.key}=${it.value}" }.joinToString("&")
// s = limit=10&offset=20&order=asc
```  
     
10. [简单获取网络图片](https://blog.csdn.net/aqi00/article/details/83833793) 
```
//获取网络上的图片验证码
    private fun getImageCode() {
        iv_image_code.isEnabled = false
        doAsync {
            val url = "$imageUrl${DateUtil.getFormatTime()}"
            val bytes = URL(url).readBytes()
            //把字节数组解码为位图数据
            val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
            //也可通过下面三行代码把字节数组写入文件，即生成一个图片文件
            val path = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/"
            val file_path = "$path${DateUtil.getFormatTime()}.png"
            File(file_path).writeBytes(bytes)
            //获得验证码图片数据，回到主线程把验证码显示在界面上
            uiThread { finishGet(bitmap) }
        }
    }

    //在主线程中显示获得到的验证码图片
    private fun finishGet(bitmap: Bitmap) {
        iv_image_code.setImageBitmap(bitmap)
        iv_image_code.isEnabled = true
    }
```

11. Fuel 貌似很简洁，比 Retrofit 更简洁，看这篇文章: [使用Fuel实现优雅的Android Kotlin网络restful请求 丢掉祖传Retrofit](https://www.jianshu.com/p/7772010dabb9)  
    明天试试用  
    
**意外的收获是： iOS 的 moya 库和这个 Fuel 那样，很简洁？**
    

# 2019-12-19
1. 单例化的第三种方式：自定义一个非空且只能一次性赋值的委托属性，  
   来自[Kotlin入门(28)Application单例化](https://blog.csdn.net/aqi00/article/details/83310069)
2. [Kotlin在Android上使用Fuel设置全局的域名和通用参数](https://www.ojit.com/article/1782586)，看看 Fuel 确实易用性上做了设计，越来越喜欢
```
FuelManager.instance.baseHeaders = mapOf(
            "Authorization" to "Bearer $ACCESS_TOKEN"
    )

    FuelManager.instance.basePath =
            "https://api.dialogflow.com/v1/"

    FuelManager.instance.baseParams = listOf(
            "v" to "20170712",                  // latest protocol
            "sessionId" to UUID.randomUUID(),   // random ID
            "lang" to "en"                      // English language
    )

    val human = ChatUser(
            1,
            "You",
            BitmapFactory.decodeResource(resources,
                    R.drawable.ic_account_circle)
    )

    val agent = ChatUser(
            2,
            "Agent",
            BitmapFactory.decodeResource(resources,
                    R.drawable.ic_account_circle)
    )

    my_chat_view.setOnClickSendButtonListener(
            View.OnClickListener {
                my_chat_view.send(Message.Builder()
                        .setUser(human)
                        .setText(my_chat_view.inputText)
                        .build()
                )

                // More code here
            }
    )
        Fuel.get("/query",
                listOf("query" to my_chat_view.inputText))
                .responseJson { _, _, result ->
                    val reply = result.get().obj()
                            .getJSONObject("result")
                            .getJSONObject("fulfillment")
                            .getString("speech")

                    my_chat_view.send(Message.Builder()
                            .setRight(true)
                            .setUser(agent)
                            .setText(reply)
                            .build()
                    )
                }
作者执行后出错了，没有拿到要的数据，原因未知，通过自己拼接 urlString 解决的
Fuel.get("https://api.dialogflow.com/v1/query?v=20150910&lang=en&query="
                +chatView.inputText+"&sessionId=12345")
```

# 2019-12-20
1. 终于到处理 JSON 数据了，google 后，安装了 JSON to Kotlin Class 插件，听说可以直接通过 JSON 字符串生成 data class  
   请看： [Android kotlin插件神器Json直接生成javaBean](https://www.cnblogs.com/zhangqie/p/11533859.html)  
2. JSON 的序列化和反序列化，有神器 Moshi 但看评测和 Kotlin 自带的 kotlinx.serialization 差不多，那还是用自带的  
   请看：[新一代Json解析库Moshi使用及原理解析](https://juejin.im/post/5bdf159251882516e246ad75) ，里面还带了执行时间的计算
```
@Serializable
data class Chinese(@Optional val age: Int = 0, @Optional val country: String? = "China") {
  @Optional
  private val hobby: String = "travel"
}

fun testKotlinXSerialize() {
    val json = JsonUtils.getJson("douban.json", this)
    val start = System.currentTimeMillis()
    val douban = JSON.parse(DoubanBean.serializer(), json)
    val end = System.currentTimeMillis()
    val consume = end - start
    val serialstart = System.currentTimeMillis()
    val seriJson = JSON.stringify(DoubanBean.serializer(), douban)
    val serizalend = System.currentTimeMillis()
    val serialConsume = serizalend - serialstart
}
```
   上面的用法有点旧，仅供参考，看下面的是官方 github 主页的简单使用：
```
import kotlinx.serialization.*
import kotlinx.serialization.json.*

@Serializable
data class Data(val a: Int, val b: String = "42")

fun main() {
    // Json also has .Default configuration which provides more reasonable settings,
    // but is subject to change in future versions
    val json = Json(JsonConfiguration.Stable)
    // serializing objects
    val jsonData = json.stringify(Data.serializer(), Data(42))
    // serializing lists
    val jsonList = json.stringify(Data.serializer().list, listOf(Data(42)))
    println(jsonData) // {"a": 42, "b": "42"}
    println(jsonList) // [{"a": 42, "b": "42"}]

    // parsing data back
    val obj = json.parse(Data.serializer(), """{"a":42}""") // b is optional since it has default value
    println(obj) // Data(a=42, b="42")
}
```

* 注意：如果用了 proguard ，要添加：  
Library should work on Android "as is". If you're using proguard, you need to add this to your proguard-rules.pro:
```
-keepattributes *Annotation*, InnerClasses
-dontnote kotlinx.serialization.SerializationKt
-keep,includedescriptorclasses class com.yourcompany.yourpackage.**$$serializer { *; } # <-- change package name to your app's
-keepclassmembers class com.yourcompany.yourpackage.** { # <-- change package name to your app's
    *** Companion;
}
-keepclasseswithmembers class com.yourcompany.yourpackage.** { # <-- change package name to your app's
    kotlinx.serialization.KSerializer serializer(...);
}
```
You may also want to keep all custom serializers you've defined.

3. 磨蹭了半天，android 里引入 kotlin.serialize 失败，最后：  
   a. 根目录的 build.gradle 的 dependents 添加 
   ```
   classpath "org.jetbrains.kotlin:kotlin-serialization:$kotlin_version"
   ``` 
   b. 继续在 app 目录下的 build.gradle 顶部 添加：
   ```
   apply plugin: 'kotlinx-serialization'
   ```
   c. 继续在 app 目录下的 build.gradle dependents 里添加：
    ```
    implementation "org.jetbrains.kotlin:kotlin-reflect:$kotlin_version" //kotlin 反射库
    implementation "org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.11.1"
    ``` 
   好多网页语焉不详，害人  

# 2019-12-21
1. 单元测试很好，要引进来，好处多，浪费点时间也算值得，具体自己搜索
2. JUnit 进行测试，参考：[Kotlin 写 Android 单元测试（二），JUnit 4 测试框架和 kotlin.test 库的使用](https://johnnyshieh.me/posts/unit-test-junit4-and-kotlin-test/)   
   看到 kotlin 也自带了测试库，那试试用  
3. 看这篇比较好：[Android 单元测试只看这一篇就够了](https://juejin.im/post/5b57e3fbf265da0f47352618)
```
单元测试要测什么？
列出想要测试覆盖的正常、异常情况，进行测试验证;
性能测试，例如某个算法的耗时等等。

单元测试的分类
本地测试(Local tests): 只在本地机器JVM上运行，以最小化执行时间，这种单元测试不依赖于Android框架，或者即使有依赖，也很方便使用模拟框架来模拟依赖，以达到隔离Android依赖的目的，模拟框架如google推荐的[Mockito][1]；
仪器化测试(Instrumented tests): 在真机或模拟器上运行的单元测试，由于需要跑到设备上，比较慢，这些测试可以访问仪器（Android系统）信息，比如被测应用程序的上下文，一般地，依赖不太方便通过模拟框架模拟时采用这种方式。
```

# 2019-12-22
1. Android 单元测试分 单java ，可模拟android类和设备，必须设备上  
   下面是依赖设备的测试，  
   运行不是每次重新运行就会重新编译，需要在类名和函数名上来回切换一下运行，才能重新编译
```
package com.booster

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.booster.utility.EasyAES
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class EasyAesInstrumentedTest {
    @Test
    fun testEncrypt() {
        // 使用 assertEquals
        var content = "..我来测试11$$\n"
        var encryptContent = "Cw+iU9l6AEt8cJx9Q5RD53WTx3DSpBcSmaagZgEDTdY="
        var encryptResult = EasyAES.encryptString256(content)
        var decryptResult = EasyAES.decryptString256(encryptResult)
        Assert.assertEquals("EasyAes 加密出错", encryptContent, encryptResult)
        Assert.assertEquals("EasyAes 解密出错", content, decryptResult)
    }
}
```
2. 选用库  testImplementation "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"
   单jvm就能测试的比较简单，有时需要模拟一些类，或android的一些类来配合测试
```
package com.booster

import android.content.Context
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

private const val FAKE_STRING = "HELLO WORLD"

@RunWith(MockitoJUnitRunner::class)
class GetRStringTest {
    @Mock
    private lateinit var mockContext: Context

    @Test
    fun readStringFromContext_LocalizedString() {
        // Given a mocked Context injected into the object under test...
        whenever(mockContext.getString(R.string.app_name)).thenReturn(FAKE_STRING) // 想测试不相等就可以返回 ("Booster")
        val getRString = GetRString(mockContext)
        // ...when the string is returned from the object under test...
        val result = getRString.getHelloWorldString()
        // ...then the result should be the expected one.
        println("$getRString = $result")
        // 期望是一样的，如果不一样，就报警后面的msg
        assertEquals(FAKE_STRING, result, "获取到的 hello world 字符串不相同")
    }
}  
```

3. 有个很奇怪的，在 test 里能用 import kotlin.test.assertEquals  
    assertEquals(FAKE_STRING, result, "获取到的 hello world 字符串不相同")  
   同样的代码，在 androidTest 里的 kotlin 文件就不行，编辑器不提示错误，编译运行时会报错：  
   Unresolved reference: test
   查了半天，没搞定，最后这一篇文章说了：[kotlin學習筆記——單元測試](https://www.itread01.com/content/1546687631.html)
```
kotlin也可以進行unit testing，如果專案中之前沒有，那麼需要做一些準備工作。
首先引入依賴
testCompile 'junit:junit:4.12'
這裡注意不能是androidTestCompile，否則會報錯Unresolved reference: xxxx
然後建立目錄
```
   解决办法是在 app 的build.gradle 里两个都加上：
```
// 引入 kotlin 的 测试库，用法参考 [Kotlin 写 Android 单元测试（二），JUnit 4 测试框架和 kotlin.test 库的使用](https://johnnyshieh.me/posts/unit-test-junit4-and-kotlin-test/)
    androidTestImplementation "org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version"  // 给 androidTest 使用
    testImplementation "org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version"         // 给 test 使用
    // Optional -- Mockito framework（可选，用于模拟一些依赖对象，以达到隔离依赖的效果）
 //   testImplementation 'org.mockito:mockito-core:3.1.0'
    androidTestImplementation "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"        // 给 androidTest 使用
    testImplementation "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"               // 给 test 使用
```

4. 